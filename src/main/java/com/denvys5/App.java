package com.denvys5;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

// Got code from Buildzoid. Reversed with Fernflower.
// For the better days, wanna improve this thing =)


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager.LookAndFeelInfo;

public class App extends JFrame {
    private JButton ClaculateButton;
    private JLabel OuputCurrent;
    private JLabel OuputWattage;
    private JTextField OverclockedClockInputTF1;
    private JTextField OverclockedVoltageInputTF1;
    private JTextField StockClockInputTF;
    private JTextField StockPowerDrawInputTF;
    private JTextField StockVoltageInputTF;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLable3;
    private JLabel jLable4;
    private JPanel jPanel1;

    public App() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLable3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.StockPowerDrawInputTF = new JTextField();
        this.StockVoltageInputTF = new JTextField();
        this.StockClockInputTF = new JTextField();
        this.OverclockedClockInputTF1 = new JTextField();
        this.jLable4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.OverclockedVoltageInputTF1 = new JTextField();
        this.jPanel1 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel8 = new JLabel();
        this.OuputWattage = new JLabel();
        this.OuputCurrent = new JLabel();
        this.ClaculateButton = new JButton();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.jLabel1.setText("Buildzoid's TDP Calculator 0.1 Alpha");
        this.jLabel2.setText("Stock Power Draw (W)");
        this.jLable3.setText("Stock Voltage (V)");
        this.jLabel4.setText("Stock Clock (MHz)");
        this.jLable4.setText("Overclocked Voltage (V)");
        this.jLabel5.setText("Overclocked Clock (MHz)");
        this.jPanel1.setBackground(new Color(220, 220, 220));
        this.jLabel6.setText("Overclocked:");
        this.jLabel7.setText("Wattage");
        this.jLabel8.setText("Current Demand");
        GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLabel6, -1, -1, 32767).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLabel7).addComponent(this.OuputWattage, -2, 64, -2)).addPreferredGap(ComponentPlacement.RELATED, 9, 32767).addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false).addComponent(this.jLabel8, -1, -1, 32767).addComponent(this.OuputCurrent, -1, -1, 32767)))).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel6).addPreferredGap(ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE).addComponent(this.jLabel7).addComponent(this.jLabel8)).addPreferredGap(ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING).addComponent(this.OuputWattage, -1, -1, 32767).addComponent(this.OuputCurrent, -1, 24, 32767)).addContainerGap()));
        this.ClaculateButton.setText("Calculate");
        this.ClaculateButton.addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent evt) {
                App.this.ClaculateButtonMouseReleased(evt);
            }
        });
        GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(Alignment.LEADING, false).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jLabel1).addGroup(layout.createSequentialGroup().addComponent(this.jLabel2).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.StockPowerDrawInputTF, -2, 50, -2)).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLable3).addComponent(this.jLabel4)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(this.StockClockInputTF, -2, 50, -2).addComponent(this.StockVoltageInputTF, -2, 50, -2))).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLable4).addComponent(this.jLabel5)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.LEADING, false).addComponent(this.OverclockedVoltageInputTF1, -1, 50, 32767).addComponent(this.OverclockedClockInputTF1, -1, 50, 32767))).addComponent(this.ClaculateButton, -1, -1, 32767)).addContainerGap(-1, 32767)));
        layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.StockPowerDrawInputTF, -2, -1, -2)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(this.jLable3).addComponent(this.StockVoltageInputTF, -2, -1, -2)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLabel4).addComponent(this.StockClockInputTF, -2, -1, -2)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(this.jLable4).addComponent(this.OverclockedVoltageInputTF1, -2, -1, -2)).addPreferredGap(ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(Alignment.LEADING).addComponent(this.jLabel5).addComponent(this.OverclockedClockInputTF1, -2, -1, -2)).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(ComponentPlacement.RELATED).addComponent(this.ClaculateButton, -1, -1, 32767).addContainerGap()));
        this.pack();
    }

    private void ClaculateButtonMouseReleased(MouseEvent evt) {
        float StockWattage = 0.0F;
        float StockVoltage = 0.0F;
        float StockClock = 0.0F;
        float OCWattage = 0.0F;
        float OCVoltage = 0.0F;
        float OCClock = 0.0F;

        try {
            StockWattage = Float.parseFloat(this.StockPowerDrawInputTF.getText());
            StockVoltage = Float.parseFloat(this.StockVoltageInputTF.getText());
            StockClock = (float)Integer.parseInt(this.StockClockInputTF.getText());
            OCVoltage = Float.parseFloat(this.OverclockedVoltageInputTF1.getText());
            OCClock = (float)Integer.parseInt(this.OverclockedClockInputTF1.getText());
        } catch (Exception var9) {
            System.out.println("User is idiot");
        }

        OCWattage = OCClock / StockClock * OCVoltage * (OCVoltage / (StockVoltage / (StockWattage / StockVoltage)));
        this.OuputWattage.setText(Float.toString(OCWattage));
        this.OuputCurrent.setText(Float.toString(OCWattage / OCVoltage));
    }

    public static void main(String[] args) {
        try {
            LookAndFeelInfo[] var1 = UIManager.getInstalledLookAndFeels();
            int var2 = var1.length;

            for (LookAndFeelInfo info : var1) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException var5) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, (String)null, var5);
        }

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new App()).setVisible(true);
            }
        });
    }
}
